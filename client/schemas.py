from typing import Union
from pydantic import BaseModel

class Client(BaseModel):
    phone_number: int
    code_operator: str
    tag: str
    timezone: str

class Response(BaseModel):
    status: int
    message: str