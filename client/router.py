from fastapi import APIRouter, Depends
from fastapi.exceptions import HTTPException
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import insert, select, update, delete
from database import get_async_session
from client.model import DBClient
from client.schemas import Client, Response
from additional import logger

router = APIRouter(
    prefix="/client",
    tags=["Client"]
)


@router.get("/get_client")
async def get_client(session: AsyncSession = Depends(get_async_session)):
    query = select(DBClient)
    result = await session.execute(query)
    clients = result.scalars().all()
    if clients is None:
        return HTTPException(
            status_code=404,
            detail='Client not found'
        )
    logger.success(
        {
            'status_code': 200,
            'message': 'Success'
        }
    )
    return {
        'status_code': 200,
        'result': clients
    }

@router.post("/add_client")
async def add_client(new_client: Client, session: AsyncSession = Depends(get_async_session)) -> Response:
    stmt = insert(DBClient).values(**new_client.dict())
    await session.execute(stmt)
    await session.commit()
    Response.status = 201
    Response.message = 'Success'
    logger.success(Response)
    return Response


@router.put("/update_client")
async def update_client(client_id: int, update_client: Client, session: AsyncSession = Depends(get_async_session)) -> Response:
    stmt = update(DBClient).where(DBClient.id == client_id).values(**update_client.dict())
    await session.execute(stmt)
    await session.commit()
    Response.status = 202
    Response.message = 'Success'
    logger.success(Response)
    return Response


@router.delete("/client_delete/{id}")
async def delete_client(client_id: int, session: AsyncSession = Depends(get_async_session)):
    query = select(DBClient).where(DBClient.id == client_id)
    get_client = await session.execute(query)
    client_data = get_client.scalar()
    await session.commit()
    print(client_data)
    if client_data is None:
        return HTTPException(
            status_code=404,
            detail='Client not found'
        )
    del_cli = await session.get(DBClient, client_id)
    await session.delete(del_cli)
    await session.commit()
    logger.success(
        {
            'status_code': 200,
            'message': 'Success'
        }
    )
    return {
        'status_code': 200,
        'result': 'Success'
    }
