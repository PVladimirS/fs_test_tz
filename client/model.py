from sqlalchemy import MetaData, Table, Column, Integer, String, TIMESTAMP, ForeignKey
from sqlalchemy.orm import relationship
from database import Base

metadata = MetaData()

# client = Table(
#     'client',
#     metadata,
#     Column('id', Integer, primary_key=True),
#     Column('phone_number', Integer, nullable=False),
#     Column('code_operator', String, nullable=False),
#     Column('tag', String),
#     Column('timezone', String),
# )


class DBClient(Base):
    __tablename__ = 'client'

    id = Column(Integer, primary_key=True)
    phone_number = Column(Integer, nullable=False)
    code_operator = Column(String, nullable=False)
    tag = Column(String)
    timezone = Column(String)
    # message = relationship(
    #     'DBClient',
    #     backref='message', passive_deletes=True
    # )
    message = relationship(
        "DBMessage",
        back_populates="client",
        cascade="all, delete",
        passive_deletes=True,
    )
