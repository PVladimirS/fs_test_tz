from pydantic import BaseModel
from datetime import datetime
from typing import Union

class Message(BaseModel):
    create_message: datetime
    status: bool
    mailing_id: int
    client_id: int

class Response(BaseModel):
    status: int
    message: str
    result: Union[list, None] = None
