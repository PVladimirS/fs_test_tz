from fastapi import APIRouter, Depends
from sqlalchemy.ext.asyncio import AsyncSession
from sqlalchemy import select, insert

from additional import logger
from database import get_async_session
from message.schemas import Message
from message.model import DBMessage

router = APIRouter(
    prefix="/message",
    tags=["Message"]
)


@router.get("/get")
async def get_message(session: AsyncSession = Depends(get_async_session)):
    query = select(DBMessage)
    result = await session.execute(query)
    all_message = result.scalars().all()
    logger.success(
        {
            'status_code': 200,
            'message': 'success'
        }
    )
    return {
        'status_code': 200,
        'response': all_message
    }


@router.post("/add_message")
async def add_message(new_message: Message, session: AsyncSession = Depends(get_async_session)):
    stmt = insert(DBMessage).values(**new_message.dict())
    await session.execute(stmt)
    await session.commit()
    logger.success(
        {
            'status_code': 201,
        }
    )
    return {
        'status_code': 201,
        'response': 'Success'
    }
