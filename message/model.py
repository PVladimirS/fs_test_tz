from sqlalchemy import MetaData, Table, Column, Integer, String, TIMESTAMP, ForeignKey, Boolean
from datetime import datetime

from sqlalchemy.orm import relationship

from database import Base

metadata = MetaData()

# message = Table(
#     'message',
#     metadata,
#     Column('id', Integer, primary_key=True),
#     Column('create_message', TIMESTAMP, default=datetime.utcnow),
#     Column('status', Boolean, nullable=False),
#     Column('mailing_id',  Integer, ForeignKey(mailing.c.id), ondelete="CASCADE"),
#     Column('client_id',  Integer, ForeignKey(client.c.id), ondelete="CASCADE"),
# )


class DBMessage(Base):
    __tablename__ = 'message'

    id = Column(Integer, primary_key=True)
    create_message = Column(TIMESTAMP, default=datetime.utcnow)
    status = Column(Boolean, nullable=False)
    mailing_id = Column(Integer, ForeignKey('mailing.id', ondelete='CASCADE'))
    client_id = Column(Integer, ForeignKey('client.id', ondelete='CASCADE'))
    mailing = relationship("DBMailing", back_populates="message")
    client = relationship("DBClient", back_populates="message")
    # message = relationship("Message", back_populates='message')
