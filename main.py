from fastapi import FastAPI
from config import REDIS_HOST, REDIS_PORT
from starlette.middleware.cors import CORSMiddleware
import aioredis
from client import router as client_router
from mailing import router as mailing_router
from message import router as message_router
from fastapi_cache import FastAPICache
from fastapi_cache.backends.redis import RedisBackend

app = FastAPI()

app.include_router(client_router)
app.include_router(mailing_router)
app.include_router(message_router)

app.add_middleware(
    CORSMiddleware,
    allow_origins=["*"],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

@app.on_event('startup')
async def startup_event():
    redis = aioredis.from_url(f"redis://{REDIS_HOST}:{REDIS_PORT}", encoding='utf-8', decode_responses=True)
    FastAPICache.init(RedisBackend(redis), prefix="fastapi-cache")