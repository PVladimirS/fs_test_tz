from pydantic import BaseModel
from typing import Union
from datetime import datetime

class Mailing(BaseModel):
    start_sending: datetime
    text_message: str
    filter: str
    stop_sending: datetime

class Response(BaseModel):
    status: int
    message: str
