from sqlalchemy import MetaData, Table, Column, Integer, String, TIMESTAMP, ForeignKey
from sqlalchemy.orm import relationship, backref

from database import Base

metadata = MetaData()

# mailing = Table(
#     'mailing',
#     metadata,
#     Column('id', Integer, primary_key=True),
#     Column('start_sending', TIMESTAMP, nullable=False),
#     Column('text_message', String, nullable=False),
#     Column('filter', String),
#     Column('stop_sending', TIMESTAMP),
# )


class DBMailing(Base):
    __tablename__ = 'mailing'

    id = Column(Integer, primary_key=True)
    start_sending = Column(TIMESTAMP, nullable=False)
    text_message = Column(String, nullable=False)
    filter = Column(String)
    stop_sending = Column(TIMESTAMP)
    message = relationship(
        "DBMessage",
        back_populates="mailing",
        cascade="all, delete",
        passive_deletes=True,
    )
