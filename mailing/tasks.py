import asyncio

from apscheduler.schedulers.background import BackgroundScheduler
import requests
from fastapi import Depends
from sqlalchemy import select
from sqlalchemy.ext.asyncio import AsyncSession

from additional import logger
from celery import Celery
from config import OAUTH_TOKEN, REDIS_PORT, REDIS_HOST, APP_HOST, APP_PORT
from database import get_async_session, get_connection
from message.model import DBMessage

celery = Celery('mailing', broker=f'redis://{REDIS_HOST}:{REDIS_PORT}')
scheduler = BackgroundScheduler()

@celery.task
def start_mailing(mail_data: dict):
    headers = {
        'Authorization': f'Bearer {OAUTH_TOKEN}'
    }
    try:
        url = f"https://probe.fbrq.cloud/v1/send/{mail_data.get('id')}"
        data = {
            'id': mail_data.get('id'),
            'phone': mail_data.get('phone_number'),
            'text': mail_data.get('text')
        }
        response = requests.post(url, headers=headers, json=data)
        if response.status_code == 200:
            logger.success(f"Status code: {response.status_code}", response.json())
            return True
        else:
            logger.warning(f"Status code: {response.status_code}")
            return False
    except Exception as Exc:
        logger.error(
            {
                'Error': Exc
            }
        )
        return {
            'Error': Exc
        }

def shedule_task(mail_data: dict):
    try:
        conn = get_connection()
        status = start_mailing(mail_data)
        stmt = f"INSERT INTO message (create_message, status, mailing_id, client_id) " \
               f"VALUES ('{mail_data.get('send_date')}', {status}, {mail_data.get('mailing_id')}, " \
               f"{mail_data.get('id')})"
        with conn.cursor() as cursor:
            cursor.execute(stmt)
            conn.commit()
            logger.success('Success background task!')
    except Exception as Exc:
        logger.error(f"Error in task send message: {Exc}")


@celery.task
def add_task(data: dict):
    date_format = '%Y-%m-%d %H:%M:%S'
    valid_date = data.get('send_date').strftime(date_format)
    data['send_date'] = valid_date
    scheduler.add_job(shedule_task, 'date', run_date=valid_date, kwargs={'mail_data': data})
    try:
        scheduler.start()
    except Exception as Exc:
        print(Exc)

# if __name__ == "__main__":
#     add_task()