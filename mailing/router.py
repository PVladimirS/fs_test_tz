from datetime import datetime
from fastapi import APIRouter, Depends
from fastapi.exceptions import HTTPException
from sqlalchemy import select, insert, delete, update
from sqlalchemy.ext.asyncio import AsyncSession
from additional import logger, statistic
from client.model import DBClient
from message.model import DBMessage
from mailing.model import DBMailing
from database import get_async_session
from mailing.schemas import Mailing, Response
from mailing.tasks import add_task

router = APIRouter(
    prefix="/mailing",
    tags=["Mailing"]
)


@router.get("/get")
async def get_mailing(session: AsyncSession = Depends(get_async_session)):
    query = select(DBMailing)
    result = await session.execute(query)
    mailings = result.scalars().all()
    if mailings is None:
        return HTTPException(
            status_code=404,
            detail='Mailing not found'
        )
    logger.success(
        {
            'status_code': 200,
        }
    )
    return {
        'status_code': 200,
        'result': mailings
    }

@router.post("/add")
async def add_mailing(new_mailing: Mailing, session: AsyncSession = Depends(get_async_session)) -> Response:
    stmt = insert(DBMailing).values(**new_mailing.dict()).returning(DBMailing)
    mailing_id = await session.execute(stmt)
    last_mailing = mailing_id.scalar()
    await session.commit()
    query_client = select(DBClient).where(
        (DBClient.tag == new_mailing.filter) | (DBClient.code_operator == new_mailing.filter)
    )
    result_client = await session.execute(query_client)
    filter_client = result_client.scalars().all()
    if new_mailing.stop_sending > datetime.now() > new_mailing.start_sending:
        for clt in filter_client:
            clt_ex = clt.__dict__
            clt_ex['text'] = last_mailing.text_message
            clt_ex['send_date'] = datetime.now()
            clt_ex['mailing_id'] = last_mailing.id
            clt_ex.pop('_sa_instance_state')
            add_task.delay(clt_ex)
    elif new_mailing.start_sending < datetime.now():
        for clt in filter_client:
            clt_ex = clt.__dict__
            clt_ex['text'] = last_mailing.text_message
            clt_ex['send_date'] = new_mailing.start_sending
            clt_ex['mailing_id'] = last_mailing.id
            clt_ex.pop('_sa_instance_state')
            add_task.delay(clt_ex)
    Response.status = 201
    Response.message = 'Success'
    logger.success(
        Response
    )
    return Response


@router.put("/update/{mailing_id}")
async def update_mailing(mailing_id: int, mailing_update: Mailing, session: AsyncSession = Depends(get_async_session)) -> Response:
    stmt = update(DBMailing).where(DBMailing.id == mailing_id).values(**mailing_update.dict())
    await session.execute(stmt)
    await session.commit()
    Response.status = 202
    Response.message = 'Success'
    logger.success(Response)
    return Response



@router.get('/get_all_statistic')
async def get_statistic(session: AsyncSession = Depends(get_async_session)):
    return_data = {
        'total_mailing': 0,
        'list_mailing': []
    }
    query_mailing = select(DBMailing)
    all_mailing = await session.execute(query_mailing)
    dict_all_mailing = all_mailing.scalars().all()
    if dict_all_mailing is None:
        return HTTPException(
            status_code=404,
            detail='Mailing not found'
        )
    return_data['total_mailing'] = len(dict_all_mailing)
    for mail in dict_all_mailing:
        mailing_info = {
            'mailing_id': 0,
            'send_mail': 0,
            'delivered_message': 0,
            'undelivered_message': 0
        }
        query_message = select(DBMessage).where(DBMessage.mailing_id == mail.id)
        all_message = await session.execute(query_message)
        statistics_data = statistic(all_message.scalars().all())
        mailing_info['mailing_id'] = mail.id
        mailing_info['send_mail'] = statistics_data.get('count')
        mailing_info['delivered_message'] = statistics_data.get('True')
        mailing_info['undelivered_message'] = statistics_data.get('False')
        return_data['list_mailing'].append(mailing_info)
    logger.success({
        'status_code': 200,
        'response': 'Success'
    })
    return return_data


@router.get('/get_statistic_once/{id_mailing}')
async def get_statistic(id_mailing: int, session: AsyncSession = Depends(get_async_session)):
    query_mailing = select(DBMailing).where(DBMailing.id == id_mailing)
    get_mailing = await session.execute(query_mailing)
    current_mailing = get_mailing.scalar()
    if current_mailing is None:
        return HTTPException(
            status_code=404,
            detail='Mailing not found'
        )
    query_message = select(DBMessage).where(DBMessage.mailing_id == current_mailing.id)
    get_message = await session.execute(query_message)
    mailing_messages = get_message.scalars().all()
    for msg in mailing_messages:
        query_client = select(DBClient).where(DBClient.id == msg.client_id)
        get_client = await session.execute(query_client)
        current_client = get_client.scalar()
        client_dict = current_client.__dict__
        msg_dict = msg.__dict__
        client_dict.pop('_sa_instance_state')
        client_dict.pop('id')
        msg_dict.pop('mailing_id')
        msg_dict.pop('client_id')
        msg_dict.update(client_dict)
    return_dict = {
        'mailing_data': current_mailing,
        'messages_data': mailing_messages
    }
    logger.success({
        'status_code': 200,
        'response': 'Success'
    })
    return return_dict

@router.delete('/delete/{id_mailing}')
async def delete_mailing(id_mailing: int, session: AsyncSession = Depends(get_async_session)) -> Response:
    stmt = delete(DBMailing).where(DBMailing.id == id_mailing)
    await session.execute(stmt)
    await session.commit()
    Response.status = 200
    Response.message = 'Success'
    logger.success(
        {
            'status_code': 200,
            'response': 'Success'
        }
    )
    return Response