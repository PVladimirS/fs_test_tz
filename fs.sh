#!/bin/bash

alembic upgrade head

gunicorn main:app --reload --timeout 9999 --workers 2 \
    --worker-class uvicorn.workers.UvicornWorker -b 0.0.0.0:8000