from loguru import logger
from pathlib import Path


logger_path = Path(__file__).parent.parent.joinpath('logging', 'logs.log')
logger.add(logger_path, rotation='1 week', compression='zip', colorize=True,
           format="{time} {level} {message}", encoding='utf-8')